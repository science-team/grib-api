grib-api (1.28.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.2.1
  * Use dh-fortran-mod >=0.7, with ${Fortran-Mod} to track version
  * Use gfortran, not gfortran-8, now default
  * Drop hard-coded xz compression

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 20 Dec 2018 16:59:27 +0000

grib-api (1.27.0-1) unstable; urgency=medium

  * New upstream release
  * Delete obsolete X-python-version statement

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 27 Jun 2018 13:15:04 +0100

grib-api (1.26.1-3) unstable; urgency=medium

  * Fix FTBFS with dpkg-buildpackage -A. Closes: #901107

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 10 Jun 2018 11:08:52 +0100

grib-api (1.26.1-2) unstable; urgency=medium

  * Force use of gfortran-8 for transition (needed to build mod files)
  * Use dh-fortran-mod to track fortran version

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 06 May 2018 11:01:41 +0100

grib-api (1.26.1-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.4
  * python-gribapi: depends on python-six. Closes: #896277
  * python3-gribapi: depends on python3-six. Closes: #896338

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 21 Apr 2018 09:55:10 +0100

grib-api (1.26.0-1) unstable; urgency=medium

  * New upstream release
  * Point VCS to salsa.debian.org

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 03 Mar 2018 16:21:02 +0000

grib-api (1.25.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.2; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 03 Jan 2018 12:34:33 +0000

grib-api (1.24.0-3) unstable; urgency=medium

  * Make grib-api-tools depend on libeccodes-data. Files needed.
    Closes: #882995
  * Mark libgrib-api-doc as M-A: foreign

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 28 Nov 2017 14:11:11 +0000

grib-api (1.24.0-2) unstable; urgency=medium

  * Remove build path from cmake, grib-api_ecbuild-config.h files for
    bit-reproducibility
  * Drop B-D on curl

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 12 Oct 2017 15:51:16 +0100

grib-api (1.24.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.1; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 04 Oct 2017 10:57:38 +0100

grib-api (1.23.1-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.0.1

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 13 Aug 2017 12:36:40 +0100

grib-api (1.23.0-3) unstable; urgency=medium

  * Remove symlink. Closes: #870583
  * Conflict: python3-gribapi and python3-eccodes

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 03 Aug 2017 07:29:45 +0100

grib-api (1.23.0-2) unstable; urgency=medium

  * Add link /usr/share/grib_api -> /usr/share/eccodes and _really_
    dont ship the data
  * Fix regression on python3.6 support in rules

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 02 Aug 2017 13:27:51 +0100

grib-api (1.23.0-1) unstable; urgency=medium

  * New upstream release
  * S-V: 4.0.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 21 Feb 2017 08:28:26 +0000

grib-api (1.19.0-1) unstable; urgency=medium

  * New upstream release
  * Make libgrib-api-tools conflict with libeccodes-tools

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 28 Nov 2016 15:43:29 +0000

grib-api (1.18.0-2) unstable; urgency=medium

  * Build --no-parallel for FTBFS on some systems

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 03 Nov 2016 21:29:42 +0000

grib-api (1.18.0-1) unstable; urgency=medium

  * New upstream release
   - openjp2 patch merged upstream
  * DH_COMPAT=10

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Oct 2016 14:03:08 +0100

grib-api (1.16.0-6) unstable; urgency=medium

  * Patch from Bas Couwenberg for library path in cmake module.
    Closes: #843634.	 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 15 Aug 2016 04:41:55 +0100

grib-api (1.16.0-5) unstable; urgency=medium

  * Sanitise cmake/* scripts more to remove all dependencies.
    Closes: #834107.
  * tools package now recommends: python*-gribapi packages.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 12 Aug 2016 11:57:44 +0100

grib-api (1.16.0-4) unstable; urgency=medium

  * openjpeg2 patch: fix compression ratio regression.
  * -dev, -tools pkgs now conflict with eccodes variants.
  * Fixes to python test failures - changes in py3 file handling

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 02 Aug 2016 19:23:41 +0100

grib-api (1.16.0-3) unstable; urgency=medium

  * Patch for openjpeg2 support.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 23 Jul 2016 10:18:18 +0100

grib-api (1.16.0-2) unstable; urgency=medium

  * Temporarily disable openjpeg support. Closes: #826816.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 22 Jul 2016 21:50:11 +0100

grib-api (1.16.0-1) unstable; urgency=medium

  * New upstream release
    - OMP patch merged upstream
  * Call dh_numpy3 in rules. 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 08 Jul 2016 11:40:37 +0100

grib-api (1.15.0-6) unstable; urgency=medium

  * Sanitize cmake files: remove python references that break magics++

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 07 Jul 2016 16:40:23 +0100

grib-api (1.15.0-5) unstable; urgency=medium

  * Patch from Chris Lamb to remove .pyc files and make build reproducible.
    Closes: #828067.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 04 Jul 2016 11:07:09 +0100

grib-api (1.15.0-4) unstable; urgency=medium

  Bugfixes to python3 support
  * Enable testing.
  Typo. Ack Closing FTBFS. Closes: #820986.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 16 Jun 2016 11:43:41 +0100

grib-api (1.15.0-3) experimental; urgency=medium

  * Add python3 support

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 12 Jun 2016 18:05:32 +0100

grib-api (1.15.0-2) unstable; urgency=medium

  * Remove png, openjpeg from cmake files so that magics++, etc. have 
    no unnecessary dependencies.

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 12 Jun 2016 18:05:18 +0100

grib-api (1.15.0-1) unstable; urgency=medium

  * New upstream release.
  * Use OpenMP rather than POSIX threads.
   - patch omp-fix.patch needed
  * Fix FTBFS on dpkg-buildpackage -A. Closes: ##820986
  * Don't call dh_buildinfo conditionally. Closes: #820990
  * Standards-Version: 3.9.8

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 27 Apr 2016 19:00:40 +0100

grib-api (1.14.7-2) unstable; urgency=medium

  * Add "|| true" to mips conditional to allow build on mips
  * Also avoid tests on hppa which can timeout
  *  Ack. which patches have been forwarded to ECMWF
  * Add examples/ to libgrib-api-dev. Closes: #574961
  * Ack. old bug fixed in previous release. Closes: #745265

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 22 Mar 2016 11:33:08 +0000

grib-api (1.14.7-1) unstable; urgency=medium

  * New upstream release.
  * Enable all tests on non-mips archs. Work on mips, but can time out on
   low-powered machines. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 20 Mar 2016 16:06:22 +0000

grib-api (1.14.5-6) experimental; urgency=medium

  * Enable all non-download tests. 
  * Add static-fix.patch to fix shadowed usage() functions. 
    Closes: #752732.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 12 Mar 2016 18:06:48 +0000

grib-api (1.14.5-6) unstable; urgency=medium

  * Set _FILE_OFFSET_BITS=64 on Hurd. (hurd.patch). Closes: #817265

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 10 Mar 2016 07:18:29 +0000

grib-api (1.14.5-5) unstable; urgency=medium

  * Fix FTBFS with -A (arch indep) builds. Closes: #817127.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 09 Mar 2016 06:51:55 +0000

grib-api (1.14.5-4) unstable; urgency=medium

  * Also conflict with libjasper-dev to ensure clean build

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 02 Mar 2016 22:32:15 +0000

grib-api (1.14.5-3) unstable; urgency=medium

  * Also remove dependencies from libgrib-api-dev on libjasper-dev.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 01 Mar 2016 18:15:44 +0000

grib-api (1.14.5-2) unstable; urgency=medium

  * Add netcdf, boost, hdf5, build-dependencies
  * disable more download depends.
   -  Disable tests until network tests issue fixed.
  * Remove jasper dependencies in favour of openjpeg;
    Jasper dropped from Debian due to security concerns
    (unsupported upstream).
  * Move to Standards-Version: 3.9.7
  * Ack. that we now buld python package. Closes: #792477.
  * Patch: Move python from distutils -> setuptools.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 29 Feb 2016 14:14:48 +0000

grib-api (1.14.5-1) experimental; urgency=medium

  * New upstream release.
  * Change maintainer as Enrico steps down. Closes: #813235.
  * Add test data package.
  * Enable tests, network tests disabled.
  * Use xz compression.
  * Move docs to grib-api-doc package.
  * Build-Conflict on libgrib-api0 to avoid errors in tests
  * Add B-D on chrpath, remove hdf5 lib paths from grib_to_netcdf

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 06 Feb 2016 07:55:46 +0000

grib-api (1.14.4-5) unstable; urgency=medium

  * Add libpng-dev, zlib1g-dev, libjpeg-dev, libjasper-dev to lgrib-api-dev
    dependencies, because cmake build scripts make compiles depend on them.
  * Add patch for Hurd compilation.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 13 Jan 2016 08:48:13 +0000

grib-api (1.14.4-4) unstable; urgency=medium

  * Don't record build paths in ecbuild_config.h. Breaks reproducibility.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 12 Jan 2016 20:18:55 +0000

grib-api (1.14.4-3) unstable; urgency=medium

  * libgrib-api-dev depends on libaec-dev as it ships dependencies in the
     cmake scripts.
  * Drop chrpath in favor of using ENABLE_RPATH=OFF in cmake
  * Build-Conflict on libgrib-api-dev, to avoid incorrect libs being
    used during testing. Set LD_LIBRARY_PATH during testing to find
    correct libs.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 12 Jan 2016 11:05:52 +0000

grib-api (1.14.4-2) unstable; urgency=medium

  * Add /usr/lib/cmake/grib_api/* files to -dev package for magics++
  * Add new binaries to /usr/bin in libgrib-api-tools

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 07 Jan 2016 04:56:40 +0000

grib-api (1.14.4-1) unstable; urgency=medium

  * New upstream release.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 05 Jan 2016 01:35:45 +0000

grib-api (1.14.3-2) unstable; urgency=medium

  * Make builds reproducible.
  * Links libs --as-needed
  * Set DISABLE_OS_CHECK=ON for Hurd, etc.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 07 Dec 2015 07:29:31 +0000

grib-api (1.14.3-1) unstable; urgency=medium

  * New upstream release
  * Switch to new ECMWF cmake-based buildsystem. Patches needed for 
    static libs, soname support.
  * Enable python-gribapi package.
  * Enable libaec support.
  * Use xz compression

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 30 Nov 2015 06:46:16 +0000

grib-api (1.13.1-3) unstable; urgency=medium

  * Rebuilt against gcc/gfortran 5 for transition. 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 07 Aug 2015 21:20:25 +0100

grib-api (1.13.1-2) unstable; urgency=medium

  * Non-maintainer upload.
  * Move to version 1.13.1 needed by CDO.
  * Update homepage again.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 07 Aug 2015 21:20:01 +0100

grib-api (1.12.3-1) experimental; urgency=medium

  * New upstream version
     - grib_api has an official soname now!
  * Updated Homepage. Closes: #746454
    Thanks: Aníbal Monsalve Salazar <anibal@debian.org>
  * Updated Standards-Version, no changes required

 -- Enrico Zini <enrico@debian.org>  Mon, 06 Oct 2014 14:58:41 +0200

grib-api (1.10.4-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Build using dh-autoreconf. Closes: #727384.

 -- Matthias Klose <doko@debian.org>  Thu, 04 Sep 2014 19:59:49 +0200

grib-api (1.10.4-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS: grib-api - tests fail for mips"
    Patch by Dragoslav Sicarov <Dragoslav.Sicarov@imgtec.com>
    Closes: #733510

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 30 Apr 2014 09:06:53 +0100

grib-api (1.10.4-3) unstable; urgency=low

  * Back to Arch: any after successful testing on mips and sparc.
    Closes: #529995.

 -- Enrico Zini <enrico@debian.org>  Sun, 25 Aug 2013 01:27:15 +0200

grib-api (1.10.4-2) unstable; urgency=low

  * Conflicts with libgrib-api-1.9.16. Closes: #720010, #720374.

 -- Enrico Zini <enrico@debian.org>  Fri, 23 Aug 2013 12:03:42 +0200

grib-api (1.10.4-1) unstable; urgency=low

  * New upstream version. Closes: #713400.
  * Updated standards-version, no changes required. No point introducing a
    symbols file since upstream sets the soname to the full library version
  * Updated debian/copyright to follow upstream's license change to Apache 2.0

 -- Enrico Zini <enrico@debian.org>  Tue, 13 Aug 2013 11:07:55 +0200

grib-api (1.10.0-1) UNRELEASED; urgency=low

  * Removed patch no-padding-test.diff, fixed upastream.
  * Removed patch fortran.diff, replaced with a different configure invocation
  * Stop using dh-autoreconf, since we do not patch the build system anymore.

 -- Enrico Zini <enrico@debian.org>  Fri, 25 Jan 2013 14:52:30 +0100

grib-api (1.9.18-1) UNRELEASED; urgency=low

  * New Upstream Version.
  * Removed patch paths.diff, accepted upstream.
  * Removed patch grib1to2.diff, accepted upstream.
  * Removed patch second_order.sh.diff, accepted upstream.

 -- Enrico Zini <enrico@debian.org>  Fri, 25 Jan 2013 14:52:30 +0100

grib-api (1.9.16-2) unstable; urgency=low

  * Conflicts with libgrib-api-1.9.9. Closes: #668697.

 -- Enrico Zini <enrico@debian.org>  Mon, 16 Apr 2012 11:25:42 +0200

grib-api (1.9.16-1) unstable; urgency=low

  * New upstream version
  * Dropped amconditional patch, no longer needed
  * Build-Depend on libpng-dev instead of libpng12-dev. Closes: #662363.
  * Updated standards-version (no changes required)

 -- Enrico Zini <enrico@debian.org>  Fri, 13 Apr 2012 14:26:20 +0200

grib-api (1.9.9-3) unstable; urgency=low

  * Build-depend on libjpeg-dev. Closes: #642832
  * padding.sh test only fails when run by a buildd, cannot reproduce
    otherwise or figure out why it fails. Disabling it for now.
    Closes: #642253
  * Removed ksh-isms on grib1to2
  * Removed ksh-ism on tests/second_order.sh

 -- Enrico Zini <enrico@debian.org>  Sun, 02 Oct 2011 09:02:17 +0200

grib-api (1.9.9-2) unstable; urgency=low

  * amconditional.patch: fixed SUBDIRS order so that everything tests need is
    built before tests are run.

 -- Enrico Zini <enrico@debian.org>  Tue, 20 Sep 2011 20:18:04 +0200

grib-api (1.9.9-1) unstable; urgency=low

  * New upstream version. Closes: #620243

 -- Enrico Zini <enrico@debian.org>  Mon, 19 Sep 2011 16:59:34 +0200

grib-api (1.9.5-1) unstable; urgency=low

  * New upstream version. Closes: #607671.
  * Build on armhf. Closes: #604659.
  * Updated standards-version (no changes required)
  * Updated soname since this release breaks ABI (see #607671)
  * libgrib-api-0d-1 Conflicts with libgrib-api-0d-0 since there is no way to
    make them coinstallable.
    Even splitting GRIB definitions in a separate package would not work,
    because definitions tend to be strongly tied to specific runtime versions,
    and two different definition packages cannot be installed at the same
    time.

 -- Enrico Zini <enrico@debian.org>  Fri, 11 Feb 2011 16:57:43 +0000

grib-api (1.9.0-2) unstable; urgency=low

  * Enable libpng support. Closes: #593216.
  * Don't use autotools-dev hooks together with dh-autoreconf

 -- Enrico Zini <enrico@debian.org>  Mon, 16 Aug 2010 13:32:12 +0100

grib-api (1.9.0-1) unstable; urgency=low

  * New upstream version. Closes: #581015
  * Added patch to fix samples and definitions install paths
  * Switch to source format 3.0 (quilt)
  * Package definitions together with the shared library like in 1.7.x,
    following upstream
  * Switch to debhelper 7

 -- Enrico Zini <enrico@debian.org>  Tue, 27 Jul 2010 11:37:44 +0100

grib-api (1.8.0-3) unstable; urgency=low

  * Added patch to fix an uninitialised memory access that makes iterator test
    fail. Closes: #570679
  * Updated standards-version (no changes required)

 -- Enrico Zini <enrico@debian.org>  Tue, 09 Mar 2010 14:43:46 +0100

grib-api (1.8.0-2) unstable; urgency=low

  * Install tools/parser as grib_parser
    Note: Fedora renames in the same way.
  * Do not install definitions: depend on libgrib-api-data instead.

 -- Enrico Zini <enrico@debian.org>  Wed, 14 Oct 2009 14:27:55 +0100

grib-api (1.8.0-1) unstable; urgency=low

  * libgrib-api-tools: changed to section utils.
  * New Upstream Version.  Closes: #540801.
  * Ported patches to new upstream version
  * Added Vcs-Git: and Vcs-Browser:

 -- Enrico Zini <enrico@debian.org>  Tue, 18 Aug 2009 12:44:03 +0100

grib-api (1.7.0-3) unstable; urgency=low

  * Limit Architecture: field to only those that work.
  * Updated Standards-Version. No changes needed.

 -- Enrico Zini <enrico@debian.org>  Sat, 04 Jul 2009 16:21:42 +0200

grib-api (1.7.0-2) unstable; urgency=low

  * Point README.source to the version provided by quilt
  * Update debian/copyright to reflect that the perl bindings follow perl's
    license

 -- Enrico Zini <enrico@debian.org>  Sun, 17 May 2009 18:58:01 +0100

grib-api (1.7.0-1) unstable; urgency=low

  * New Upstream Version.  Closes: #519184
  * Ported patches to new upstream version
  * Rebuild the build system in debian/rules
  * debian/rules clean: delete files modified by autotools
  * Added debian-specific release to the library name, so we will not conflict
    with upstream once they will start shipping shared libraries.
  * Updated Standards-Version
     - Renamed libgribapi-bin to libgribapi-tools as suggested by policy
  * Use debhelper 7
  * Added manpages for those commands that have any documentation

 -- Enrico Zini <enrico@debian.org>  Wed, 22 Apr 2009 15:47:53 +0100

grib-api (1.6.4-1) unstable; urgency=low

  * Initial Debian release
  * Patches:
     - amconditional: Use automake conditionals to exclude subdirectories
     - libtool: Create a shared library
     - fortran: Fixes for fortran packaging

 -- Enrico Zini <enrico@debian.org>  Tue, 18 Nov 2008 11:15:52 +0000
